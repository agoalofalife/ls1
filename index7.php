<?php
/**
 * Created by PhpStorm.
 * User: FitzgeraldFox
 * Date: 24.02.2017
 * Time: 16:08
 */
$output = "<table>
            <tbody>";
for ($i = 1; $i < 11; $i++) {
    $output .= "<tr>";
    for ($j = 1; $j < 11; $j++) {
        $output .= "<td>";
        if (($i % 2 == 0) && ($j % 2 == 0)) {
            $output .= "(".$i*$j.")";
        } elseif (($i % 2 == 1) && ($j % 2 == 1)) {
            $output .= "[".$i*$j."]";
        } else {
            $output .= $i*$j;
        }
        $output .= "</td>";
    }
    $output .= "</tr>";
}
$output .= "</tbody>
        </table>";
echo $output;
